﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball3 : MonoBehaviour
{
    public Vector2 velocity;

    private Rigidbody2D rigid;

    void Start(){
        rigid = GetComponent<Rigidbody2D>();

        rigid.AddForce(velocity, ForceMode2D.Impulse);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Muro")
        {
            Debug.LogWarning("Hemos chocado contra un muro");

            Vector2 velocidadActual = rigid.velocity;
            velocidadActual.y = velocidadActual.y * -20;
            Debug.LogWarning(velocidadActual);
            rigid.velocity = new Vector2(velocidadActual.x, velocidadActual.y);
            //rigid.AddForce(velocidadActual, ForceMode2D.Impulse);
        }
    }
}
