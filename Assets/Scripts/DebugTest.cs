﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTest : MonoBehaviour
{
    //Propiedad
    public string debugString;
    public int numero;

    void Awake(){
        
        Debug.Log(debugString);

    }

    void Reset()
    {
        //Output the message to the Console
        Debug.Log("Reset");
        debugString = "Hola caracula";
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 1);
    }

    private void OnEnable() {
        Debug.Log("OnEnable");
    }

    private void OnDisable() {
        Debug.Log("OnDisable");
    }

    private void Start() {
        Debug.Log("Start");
    }

    public void PintaTonterias(){
        Debug.Log("Pinto tonterias "+ numero);
    }
}
