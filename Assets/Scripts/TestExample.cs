﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestExample : MonoBehaviour
{
    private Test[] arrayTest;


    void Start()
    {
        arrayTest = new Test[100];

        for(int i=0; i<100;i++){
            arrayTest[i] = new Test();
            arrayTest[i].intVariable = i;
            Test.staticVariable = i;
            Debug.Log(arrayTest[i].intVariable);
        }

        Debug.LogError(Test.staticVariable);
        Debug.LogError(Test.staticVariable);
    }

}
