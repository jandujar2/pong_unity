﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaseDerivada : ClaseBase
{
    public float dec;
    public Color color;

    protected override void Start()
    {
        base.Start();

        Metodo3();
    }

    // Override: sobreescribir o extender un método de la clase base.
    protected override void Metodo()
    {
        base.Metodo(); //Extender

        Debug.Log("CLASE DERIVADA");
    
    }

    public void Metodo3()
    {
        entero = 8888;
        dec = 1.799f;
        EscribirTexto("Hola k ase");
        Debug.Log(LeerTexto());
        //texto = "texto";
        Metodo2();
    }
}
