﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase: Define los miembros de un objeto.
// Objeto: instancia de una clase.

// Clase
public class ClaseEjemplo
{
    // CAMPOS (variables)
    public int numero;
    public string texto;

    // static: campo que comparten todas las instancias de una clase
    public static bool verdad;

    // PROPIEDAD
    public float dec { get; private set; }

    // METODOS (funciones)
    public void Metodo()
    {

    }
    public bool Metodo2()
    {
        return false;
    }
    private void Metodo3()
    {

    }

    // public: Accesible desde fuera de la clase. PARA TODOS.
    // private: Solo es accesible desde la propia clase. PRIVADA PARA TODOS LOS DEMÁS.

    // Miembros de la clase: propiedades, campos y metodos.
}
