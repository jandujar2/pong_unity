﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaseBase : MonoBehaviour
{
    protected int entero;
    private string texto;
    public bool booleano;

    protected virtual void Start()
    {
        Metodo();
    }

    // Virtual: permite sobreescribir el método desde una clase derivada.
    protected virtual void Metodo()
    {
        Debug.Log("CLASE BASEEE");
    }
    public void Metodo2() { }

    public string LeerTexto()
    {
        return texto;
    }
    public void EscribirTexto(string t)
    {
        texto = t;
    }
    // protected: Privado para todos, excepto para las clases derivadas.
}
