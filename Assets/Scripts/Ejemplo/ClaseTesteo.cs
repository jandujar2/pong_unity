﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaseTesteo
{
    public ClaseEjemplo ejemplo;
    void MetodoTest()
    {
        ejemplo = new ClaseEjemplo();

        ejemplo.numero = 1;
        ejemplo.Metodo();

        ClaseEjemplo ejemplo2 = new ClaseEjemplo();

        ClaseEjemplo.verdad = false;        
    }

    void MetodoHerencia()
    {
        ClaseDerivada d = new ClaseDerivada();
        //d.entero = 0;
        d.dec = 1.0f;

        ClaseBase b = new ClaseBase();
        //b.entero = 0;
       
    }
}
